package distsys;

import java.io.Serializable;
import java.sql.Timestamp;

// Message from client towards Mapper
public class MapTask implements Serializable {

    private static final long serialVersionUID = 7L;
    private final double Lat1;
    private final double Long1;
    private final double Lat2;
    private final double Long2;
    private final Timestamp t_min, t_max;

    public MapTask(double Lat1, double Long1, double Lat2, double Long2, Timestamp t_min, Timestamp t_max) {
        this.Lat1 = Lat1;
        this.Long1 = Long1;
        this.Lat2 = Lat2;
        this.Long2 = Long2;
        this.t_min = t_min;
        this.t_max = t_max;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public double getLat1() {
        return Lat1;
    }

    public double getLong1() {
        return Long1;
    }

    public double getLat2() {
        return Lat2;
    }

    public double getLong2() {
        return Long2;
    }

    public Timestamp getT_min() {
        return t_min;
    }

    public Timestamp getT_max() {
        return t_max;
    }

    @Override
    public String toString() {
        return "MapTask{" + "Lat1=" + Lat1 + ", Long1=" + Long1 + ", Lat2=" + Lat2 + ", Long2=" + Long2 + ", t_min=" + t_min + ", t_max=" + t_max + '}';
    }
    

}
