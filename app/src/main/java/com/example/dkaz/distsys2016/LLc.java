package com.example.dkaz.distsys2016;
import distsys.*;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class LLc implements Serializable {

    private static final long serialVersionUID = 6246L;

    private final double Lat,Long;

    public LLc(double Lat, double Long) {
        this.Lat = Lat;
        this.Long = Long;

    }

    public double getLat() {
        return Lat;
    }

    public double getLong() {
        return Long;
    }

    @Override
    public String toString() {
        return "com.example.dkaz.distsys2016.LL{" + "Lat=" + Lat + ", Long=" + Long + '}';
    }

    public LatLng getLatLng(){
        return new LatLng(Lat,Long);
    }

    public LL getLL(){
        return new LL(getLat(),getLong());
    }


}