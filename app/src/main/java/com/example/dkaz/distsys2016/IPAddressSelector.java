package com.example.dkaz.distsys2016;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import distsys.*;

import java.util.HashMap;

public class IPAddressSelector extends AppCompatActivity {
    private String[] ipM = new String[3];
    private String ipR;
    private int mPort;
    private int rPort;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipaddress_selector);
        final MapTaskc m = (MapTaskc) getIntent().getSerializableExtra("MapTaskc");
        final EditText m1 = (EditText)findViewById(R.id.m1IP);
        m1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m1.requestFocus();
            }
        });
        m1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ipM[0] = s.toString();
            }
        });


        final EditText m2 = (EditText)findViewById(R.id.m2IP);
        m2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ipM[1] = s.toString();
            }
        });
        m2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m2.requestFocus();
            }
        });


        final EditText m3 = (EditText)findViewById(R.id.m3IP);
        m3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m3.requestFocus();
            }
        });
        m3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ipM[2] = s.toString();
            }
        });
        final EditText rd = (EditText)findViewById(R.id.rIP);
        rd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ipR = s.toString();
            }
        });
        rd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rd.requestFocus();
            }
        });
        final EditText mp = (EditText) findViewById(R.id.mPort);
        mp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.requestFocus();
            }
        });
        mp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    mPort = Integer.parseInt(s.toString());
                } catch (Exception e){

                }
            }
        });
        final EditText rp = (EditText)findViewById(R.id.rPort);
        rp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rp.requestFocus();
            }
        });
        rp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    rPort = Integer.getInteger(s.toString());
                } catch (Exception e){

                }
            }
        });
        Button done = (Button)findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  DEBUG
                   // HashMap map = new HashMap<com.example.dkaz.distsys2016.LL,Integer>();
                   // map.put(new com.example.dkaz.distsys2016.LL(23.0,12.3),13);
                   // map.put(new com.example.dkaz.distsys2016.LL(23.1,12.3),12);
                   // Intent i = new Intent(getApplicationContext(),MainActivity.class);
                    //i.putExtra("map",map);
                    //startActivity(i);

                    String ipM[] = {"192.168.42.5"};

                    Client c = new Client(1,ipM,"192.168.42.5",3000,4000);
                    //Client c = new Client(1,ipM,ipR,mPort,rPort);
                    c.prepareRequest(m);
                    c.execute();
                    HashMap map = new HashMap<LL,Integer>();
                    Intent i = new Intent(getApplicationContext(),MainActivity.class);
                    i.putExtra("map",map);
                    startActivity(i);

            }
        });
    }
}
