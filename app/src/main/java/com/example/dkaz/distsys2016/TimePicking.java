package com.example.dkaz.distsys2016;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class TimePicking extends FragmentActivity {

    private Timestamp timestampMin, timestampMax;
    private MapTaskc m;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_picking);
        m = (MapTaskc) getIntent().getSerializableExtra("MapTaskc");
        timestampMax = new Timestamp(Calendar.getInstance().getTime().getTime());
        timestampMin = timestampMax;
        final EditText dateMin = (EditText) findViewById(R.id.DateTimeMin);
        dateMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateMin.requestFocus();
            }
        });
        dateMin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                timestampMin = new Timestamp(date(s.toString()));
                m.setT_min(timestampMin);
            }

        });
        final EditText dateMax = (EditText) findViewById(R.id.DateTimeMax);
        dateMax.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dateMax.requestFocus();
            }
        });
        dateMax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                timestampMax = new Timestamp(date(s.toString()));
                m.setT_max(timestampMax);
            }
        });
        Button next = (Button)findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),IPAddressSelector.class);
                i.putExtra("MapTaskc",m);
                startActivity(i);
            }
        });
    }

    public long date(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(date).getTime();
        } catch (ParseException pex) {
            /*
                DEBUG
                Toast t = Toast.makeText(getApplicationContext(),date+" is in invalid formnat",1);
                t.show();
            */
            return 0;
        }
    }
}
