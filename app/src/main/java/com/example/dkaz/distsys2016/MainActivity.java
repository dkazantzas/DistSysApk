package com.example.dkaz.distsys2016;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import distsys.*;
import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap gMap;
    private ArrayList<Marker> markers =new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MapFragment m = (MapFragment) getFragmentManager().findFragmentById(R.id.Mapfragment);
        m.getMapAsync(this);

        Button b = (Button) findViewById(R.id.sbutton);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLngBounds l = gMap.getProjection().getVisibleRegion().latLngBounds;

                Intent i = new Intent(getApplicationContext(), TimePicking.class);
                i.putExtra("MapTaskc", new MapTaskc(l));
                startActivity(i);
            }
        });
        Button clear = (Button)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gMap.clear();
                markers.clear();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        UiSettings uiSettings = gMap.getUiSettings();
        uiSettings.setAllGesturesEnabled(true);
        uiSettings.setMapToolbarEnabled(true);
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setCompassEnabled(true);
        if (getIntent().hasExtra("map")){
            createMarkers();
        }
    }

    public void createMarkers(){
        HashMap<LL,Integer> map = new HashMap();
        map.putAll((HashMap<LL,Integer>)getIntent().getSerializableExtra("map"));
        LL tmp = new LL(0.0,0.0);
        for(LL l: map.keySet()){
            MarkerOptions mo = new MarkerOptions();
            mo.position(new LatLng(l.getLat(),l.getLong()));
            mo.title(map.get(l).toString());
            if (mo!=null) {
                markers.add(gMap.addMarker(mo));
            }
            tmp = l;
        }
        getIntent().removeExtra("map");
        getIntent().removeExtra("MapTaskc");

        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(tmp.getLat(),tmp.getLong()),7));
    }


}
