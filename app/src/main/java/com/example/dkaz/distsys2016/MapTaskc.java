package com.example.dkaz.distsys2016;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.Serializable;
import java.sql.Timestamp;
import distsys.*;
// Message from client towards Mapper
public class MapTaskc implements Serializable {

    private static final long serialVersionUID = 7L;
    private double Lat1;
    private double Long1;
    private double Lat2;
    private double Long2;
    private Timestamp t_min, t_max;

    public void setLat1(double lat1) {
        Lat1 = lat1;
    }

    public void setLong1(double long1) {
        Long1 = long1;
    }

    public void setLat2(double lat2) {
        Lat2 = lat2;
    }

    public void setLong2(double long2) {
        Long2 = long2;
    }

    public void setT_min(Timestamp t_min) {
        this.t_min = t_min;
    }

    public void setT_max(Timestamp t_max) {
        this.t_max = t_max;
    }

    public MapTaskc(double Lat1, double Long1, double Lat2, double Long2, Timestamp t_min, Timestamp t_max) {
        this.Lat1 = Lat1;
        this.Long1 = Long1;
        this.Lat2 = Lat2;
        this.Long2 = Long2;
        this.t_min = t_min;
        this.t_max = t_max;
    }

    public MapTaskc(LatLngBounds llb){
        Lat2 = llb.northeast.latitude;
        Long2 = llb.northeast.longitude;
        Lat1 = llb.southwest.latitude;
        Long1 = llb.southwest.longitude;

    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public double getLat1() {
        return Lat1;
    }

    public double getLong1() {
        return Long1;
    }

    public double getLat2() {
        return Lat2;
    }

    public double getLong2() {
        return Long2;
    }

    public Timestamp getT_min() {
        return t_min;
    }

    public Timestamp getT_max() {
        return t_max;
    }

    @Override
    public String toString() {
        return "MapTaskc{" + "Lat1=" + Lat1 + ", Long1=" + Long1 + ", Lat2=" + Lat2 + ", Long2=" + Long2 + ", t_min=" + t_min + ", t_max=" + t_max + '}';
    }

    public MapTask getMapTask(){
        return new MapTask(getLat1(),getLong1(),getLat2(),getLong2(),getT_min(),getT_max());
    }
}
